#pragma once

#ifndef _CRYPT_HPP
#define _CRYPT_HPP

#include <gcrypt.h>

/**
 * Initializes the library and configures the key
 */
static ret_code_t crypto_init();

/**
 * Gracefully terminates the crypto library
 */
static ret_code_t crypto_terminate();

#endif
