#include <iostream>
#include "common.hpp"
#include "audio.hpp"

/**
 * Entry point
 */
int main(void)
{
  // Initialize audio library
  if(audio_init() != RET_OK)
  {
    std::cerr << "Could not initialize audio library" << std::endl;
    exit(1);
  }

  // Initialize crypto library
  if(crypto_init() != RET_OK)
  {
    std::cerr << "Could not initialize crypto library" << std::endl;
    exit(1);
  }

  // Start infinite loop
  while(true)
  {

  }

  // Terminate gracefully
  audio_terminate();

  crypto_terminate();

    return 0;
}
