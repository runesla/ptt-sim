#include <portaudio.h>
#include "common.hpp"
#include "audio.hpp"

static ret_code_t audio_init()
{
    PaError retcode;

    // Initialize PortAudio
    if((retcode = Pa_Initialize()) != paNoError)
    {
        printf("PortAudio error: %s\n", Pa_GetErrorText(retcode));
        return RET_NOK;
    }
    printf("PortAudio initialized\n");

    // Open an I/O stream
    //PaStream* stream;

    return RET_OK;
}

static int paCallback(const void* input_buffer,
                      void* output_buffer,
                      const PaStreamCallbackTimeInfo* time_info,
                      PaStreamCallbackFlags status_flags,
                      void* user_data)
{
    paData* data = (paData*) user_data;
    float* out = (float*) output_buffer;
    (void) input_buffer;

    for(unsigned int i = 0; i < frames_per_buffer; i++)
    {
        out++ = data->left_phase;
        out++ = data->right_phase;

        // TODO
    }

    return 0;
}

static ring_buffer_size_t rbs_min(ring_buffer_size_t a, ring_buffer_size_t b)
{
    return (a < b) ? a : b;
}

static int write_raw_file(void* ptr)
{
    paData* data = (paData*) ptr;

    // Mark thread started
    data->threadSyncFlag = 0;

    while(1)
    {
        ring_buffer_size_t elements_in_buffer = PaUtil_GetRingBufferReadAvailable(&data->ringBuffer);

        if((elements_in_buffer >= data->ringBuffer.bufferSize / NUM_WRITES_PER_BUFFER) || data->threadSyncFlag)
        {
            void* ptr[2] = { 0 };
            ring_buffer_size_t sizes[2] = { 0 };
        
            // Read directly from the ring buffer
            ring_buffer_size_t elements_read = PaUtil_GetRingBufferReadRegions(
                                                                              &data->ringBuffer,
                                                                              elements_in_buffer,
                                                                              ptr + 0,
                                                                              sizes + 0,
                                                                              ptr + 1,
                                                                              sizes + 1);

            if(elements_read > 0)
            {
                for(unsigned int i = 0; i < 2 && ptr[i] != NULL; ++i)
                {
                    fwrite(ptr[i], data->ringBuffer.elementSizeBytes, sizes[i], data->file);
                }

                PaUtil_AdvanceRingBufferReadIndex(&data->ringBuffer, elementsRead);
            }

            if(data->threadSyncFlag)
            {
                break;
            }
        }

        Pa_Sleep(20);
    }

    data->threadSyncFlag = 0;

    return 0;
}

static int select_device(PaStream* stream)
{
    // Query devices
    int numDevices;
    numDevices = Pa_GetDeviceCount();

    if(numDevices < 0)
    {
        printf("ERROR: No devices found - return code: 0x%x\n", numDevices);
        return -1;
    }

    const PaDeviceInfo* deviceInfo;

    for(unsigned int i = 0; i < numDevices; i++)
    {
        deviceInfo = Pa_GetDeviceInfo(i);
        printf("Device: \t%s\n", deviceInfo->name);
    }
    
    // If only one device is found, it is the default
    if(numDevices == 1)
    {
        if((retcode = Pa_OpenDefaultStream(&stream,         
                                           0,               /* no input channels */
                                           2,               /* stereo output */
                                           paFloat32,       /* 32 bit floating point output */
                                           SAMPLE_RATE,
                                           256,             /* frames per buffer -- the number of sample frames that PA will request from the callback */
                                           paCallback,
                                           &data)) != paNoError)
        {
            printf("PortAudio error: %s\n", Pa_GetErrorText(retcode));
            return -1;
        }

    }
    else
    {
        int selection;

        do
        {
            printf("Select device: ");
            sscanf(stdin, selection);

        } while(selection > numDevices || numDevices < numDevices);

        // TODO: set device
    }
}

static void capture()
{

}

static void playback()
{

}

static ret_code_t audio_terminate()
{
    if((retcode = Pa_Terminate()) != paNoError)
    {
        printf("PortAudio error: %s\n", Pa_GetErrorText(retcode));
        return RET_NOK;
    }
    printf("PortAudio terminated\n");
    return RET_OK;
}
