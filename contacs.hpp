#pragma once

#include <string.h>
#include "common.hpp"

#ifndef _CONTACTS_HPP
#define _CONTACTS_HPP

class Contact
{
public:
  Contact(std::string name, std::string number, std::string public_key) : _name(name), _number(number), _public_key(public_key)
  static void update(const Contact& contact);
  ~Contact();
private:
  std::string _name;
  std::string _number;
  std::string _public_key;
};

#endif
