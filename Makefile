CC = gcc
CFLAGS = -O1 -Wall -g
LDFLAGS = -lm -lrt -lportaudio -lm -lasound -ljack -pthread -lgcrypt -lgpg-error

SOURCES = $(wildcard *.cpp)
OBJECTS = $(SOURCES:.cpp=.o)
DEPENDENCIES = $(addprefix .,$(SOURCES:.cpp=.d))
#PORTAUDIO = libportaudio.a

.PHONY: clean all

all: main

main: main.c
	$(CC) $^ $(CFLAGS) $(LDFLAGS) -o $@

clean:
	$(RM) main $(OBJECTS) $(DEPENDENCIES)

-include $(DEPENDENCIES)
