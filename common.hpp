#pragma once

// TODO: In the future, replace with a common JSON structure with pre-determined fields and expected values
// that is used to set these values. Keep in mind that these values must reside in the system as macros (much
// like the way Mbed OS solves it)

#ifndef _COMMON_HPP
#define _COMMON_HPP

typedef enum
{
 RET_OK = 0,
 RET_NOK = 1

} ret_code_t;

#define CONTACTS_FILE_PATH "/contacts.dat"
#ifndef CONTACTS_FILE_PATH
#error "Program requires an absolute path to the contacts list"
#endif

#define PUBKEY_FILE_PATH "/pub.pem"
#ifndef PUBKEY_FILE_PATH
#error "Program requires an absolute path to your public key"
#endif

#define PRIVKEY_FILE_PATH "/key.pem"
#ifndef PRIVKEY_FILE_PATH
#error "Program requires an absolute path to your private key"
#endif

#endif
