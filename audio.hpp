#pragma once

#ifndef _AUDIO_HPP
#define _AUDIO_HPP

//https://github.com/PortAudio/portaudio/blob/master/examples/paex_record_file.c

#include "portaudio.h"
//#include "pa_ringbuffer.h"
//#include "pa_util.h"

typedef struct
{
    unsigned frameIndex;
    int threadSyncFlag;
    SAMPLE* ringBufferData;
    PaUtilRingBuffer ringBuffer;
    FILE* file;
    void* threadHandle;

} paData;

static constexpr int SAMPLE_RATE = 44100;

/**
 * Routine for initialized the PortAudio library
 */
static int audio_init();

/**
 * Callback function for audio capture
 */
static int paCallback(const void* inputBuffer, void* outputBuffer, const PaStreamCallbackTimeInfo* timeInfo, PaStreamCallbackFlags statusFlags, void* userData);

/**
 * Get the smaller buffer
 */
static ring_buffer_size_t rbs_min(ring_buffer_size_t a, ring_buffer_size_t b);

/**
 * Write data from the ring buffer into a file while recording
 */
static int write_raw_file(void* ptr);

/**
 * Select audio device
 */
static void select_device(PaStream* stream);

/**
 * Start audio capture
 */
static void capture();

/**
 * Start audio playback
 */
static void playback();

/**
 * Gracefully terminate PortAudio
 */
static void audio_terminate();

#endif
